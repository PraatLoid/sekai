#ifndef VOCODER
#define VOCODER
#include <vector>
#include "sekai/hzosc.h"
#include <sndfile.h>

#define MAX_POLYPHONY 8
#define MAX_PEAKS 500
#define MAX_HARM 100

struct Input
{
	float* buffer;
	int pos;
	float* magn;
	float* freq;
	bool valid;
	int peaks[MAX_PEAKS];
	int peaks_count;
};

#define INPUT_COUNT 4

class Vocoder
{
	public:
	Vocoder();
	~Vocoder();
	void init(int samplerate,int buffer_size);
	void noteOn(int notenum,int velocity);
	void noteOff(int notenum);
	void controllerEvent(int a1,int a2);
	void pitchBend(int a1,int a2);
	void scan(float* samples,int count);
	void fill(float* samples,int count);
	void shutdown();
	private:
	int samplerate=0;
	int buffer_size=0;
	FILE* debugfile=0;
	IFFTOsc* ifft=nullptr;
	SNDFILE *recfile; 
	bool running;
	
	//input processing
	Input inputs[INPUT_COUNT];
	int input_pos=0;
	
	//FFT
	fftw_plan forward_r2c;
	fftw_complex *spectrum;
	double *waveform;
	double *window;
	
	//tmp
	float* last_phase;
	float* x;
	float* y;
	
	//midi input
    char key[MAX_POLYPHONY];
    float freq[MAX_POLYPHONY];
    int active_keys=0;	
	float bend=0;	
};
#endif
