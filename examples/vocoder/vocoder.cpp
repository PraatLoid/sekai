#include "vocoder.hpp"
#include <fstream>
#include <iostream>

#include <assert.h>
#include <math.h>
#include <sekai/common.h>
#include <sekai/mfcc.h>

#define TWOPI (2 * M_PI)
#define IMP_LENGTH_MAX (1024 * 8)


using namespace std;

extern std::string basedir;

double midi_freq(float m) {
  /* converts a MIDI note number to a frequency
     <http://en.wikipedia.org/wiki/MIDI_Tuning_Standard> */
  return 440.0 * pow(2, (double)(m - 69.0) / 12.0);
}


Vocoder::Vocoder() {}
Vocoder::~Vocoder() {}
void Vocoder::init(int samplerate, int buffer_size) {
  this->samplerate = samplerate;
  this->buffer_size = buffer_size;
  debugfile=fopen("/tmp/vocoder.debug","w");
  ifft = new IFFTOsc(buffer_size*2,buffer_size);
  ifft->fs = samplerate;
  int fft_size = buffer_size*INPUT_COUNT;
  for(int i=0;i<INPUT_COUNT;i++)
  {
	  inputs[i].buffer = new float[buffer_size];
	  inputs[i].pos=0;
	  inputs[i].magn = new float[fft_size/2];
	  inputs[i].freq = new float[fft_size/2];
	  inputs[i].valid=0;
  }
  
  waveform = new double[fft_size];
  window = new double[fft_size];
  spectrum = new fftw_complex[fft_size];
  for(int i=0;i<fft_size;i++)
  {
	window[i] = -.5*cos(2.*M_PI*(double)i/(double)fft_size)+.5;
  }
  forward_r2c = fftw_plan_dft_r2c_1d(fft_size, waveform,spectrum, FFTW_ESTIMATE);
  last_phase = new float[fft_size/2];
  x = new float[fft_size/2];
  y = new float[fft_size/2];
  for (int i=0;i<MAX_POLYPHONY;i++)
  {
	  key[i]=0;
	  freq[i]=0;
  }
  
  SF_INFO info;
  info.format = SF_FORMAT_WAV | SF_FORMAT_PCM_16;
  info.channels = 1;
  info.samplerate = samplerate;  
  recfile = sf_open ("/tmp/sekai_vocoder.wav", SFM_WRITE, &info);
  
}

void Vocoder::shutdown()
{
	if(recfile) {
		printf("closing recfile\n");
		sf_close(recfile);
		printf("closed recfile\n");
	}
}


void Vocoder::noteOn(int notenum, int velocity) {
  for (int i = 0; i < MAX_POLYPHONY; i++) {
    if (key[i] == 0) {
      freq[i] = midi_freq(notenum + bend);
      key[i] = notenum;
      active_keys++;
      break;
    }
  }
}
void Vocoder::noteOff(int notenum) {
  for (int i = 0; i < MAX_POLYPHONY; i++) {
    if (key[i] == notenum) {
      freq[i] = 0;
      key[i] = 0;
      active_keys--;
      break;
    }
  }
  printf("noteOff %i %i\n",notenum,active_keys);
}

void Vocoder::pitchBend(int a1, int a2) {
  bend = 1.0 * (a2 * 128 + a1 - 8 * 1024) / (8 * 1024);
  for (int i = 0; i < MAX_POLYPHONY; i++) {
    if (freq[i]) freq[i] = midi_freq(key[i] + bend);
  }
}

void Vocoder::fill(float *buffer, int size) {
  
  auto input = inputs[input_pos%INPUT_COUNT];
  if(input.valid)
  {
	  x[0]=0;
	  y[0]=0;
	  int i;
	  for(i=0;i<input.peaks_count;i++)
	  {
		  x[i+1] = input.freq[input.peaks[i]];
		  y[i+1] = input.magn[input.peaks[i]];
	  }
	  x[i+1] = samplerate/2;
	  y[i+1] = 0;
	  
	  int k=0;
	  
	  
	  for (int j = 0; j < MAX_POLYPHONY; j++) {
		  
		  if(freq[j])
		  {
			  //printf("active keys=%i\n",active_keys);
			  for(int h=1;h<MAX_HARM;h++)
			  {
				float magn = interp_linear(x,y,i+2,freq[j]*h);
				ifft->amp[k] = magn/MAX_HARM/active_keys;
				ifft->frq[k] = freq[j]*h;
				k++;
			  }
		  }
		  
	  }
	  
	  while(k<buffer_size)
	  {
		 ifft->amp[k]=0;
		 ifft->frq[k]=0;
		 k++;
	  }
  }
 	
  ifft->processOneFrame ();
  
  for (int i = 0; i < size; i++) {
	  if(active_keys > 0)
	  {
		buffer[i] = ifft->output_buffer[i] * 0.1 / active_keys;
		if(fabs(buffer[i])>0.99) printf("clipping %f active_keys=%i\n",fabs(buffer[i]),active_keys);
	  }
	  else
	  {
		buffer[i] = ifft->output_buffer[i] * 0.1;
		if(fabs(buffer[i])>0.99) printf("clipping %f\n",fabs(buffer[i]));
	  }
  }
  
  sf_write_float (recfile, buffer, size);
}
void Vocoder::scan(float *buffer, int size) {
	
	inputs[input_pos%INPUT_COUNT].pos = input_pos;
	for (int i = 0; i < size; i++) 
		inputs[input_pos%INPUT_COUNT].buffer[i] = buffer[i];
	input_pos++;
	
	//	#define SF_DEBUG
	
	//copy in and window
	if(input_pos>=INPUT_COUNT)
	{
		#ifdef SF_DEBUG
		SF_INFO info;
		info.format = SF_FORMAT_WAV | SF_FORMAT_PCM_16;
		info.channels = 1;
		info.samplerate = samplerate;
		char fn[1024];
		sprintf(fn,"/tmp/vocoder_%i.wav",input_pos);
		// Open sound file for writing
		SNDFILE *outfile = sf_open (fn, SFM_WRITE, &info);
		#endif
		
		fprintf(debugfile,"HAVE_INPUT");
		for(int i=0; i<INPUT_COUNT; i++)
		{
			int pos = inputs[(input_pos+i)%INPUT_COUNT].pos;
			for(int j=0;j<size;j++)
			{
				waveform[j+i*size] = inputs[(input_pos+i)%INPUT_COUNT].buffer[j] * window[j+i*size];
			} 
			fprintf(debugfile," %i",pos);
		}
		
		fprintf(debugfile,"\n");
		
		fftw_execute(forward_r2c);
		
		int fft_size = buffer_size*INPUT_COUNT;
		float freq_per_bin = samplerate/(double)fft_size;
		
		for(int k=0;k<fft_size/2;k++)
		{
			float real = spectrum[k][0];
			float imag = spectrum[k][1];
		
			float magn = 2.*sqrt(real*real + imag*imag);
			float phase = atan2(imag,real);
			
			float tmp = phase - last_phase[k];
			last_phase[k] = phase;
			
			float expct = 2.*M_PI*1.0/INPUT_COUNT;
			tmp -= (double)k*expct;
			
			int qpd = tmp/M_PI;
			if (qpd >= 0) qpd += qpd&1;
			else qpd -= qpd&1;
			tmp -= M_PI*(double)qpd;
			
			tmp = INPUT_COUNT*tmp/(2.*M_PI);
			tmp = (double)k*freq_per_bin + tmp*freq_per_bin;
			
			//if(magn>1)
			//fprintf(debugfile,"%i: magn=%f freq=%f\n",k,magn,tmp);
			inputs[input_pos%INPUT_COUNT].magn[k] = magn;
			inputs[input_pos%INPUT_COUNT].freq[k] = tmp;

		}
		//frame is valid here
		int peak_pos=0;
		for(int k=5;k<fft_size/2-5;k++)
		{
			float ref = inputs[input_pos%INPUT_COUNT].magn[k];
			if(ref> inputs[input_pos%INPUT_COUNT].magn[k-1] &&
			   ref> inputs[input_pos%INPUT_COUNT].magn[k-2] &&
			   ref> inputs[input_pos%INPUT_COUNT].magn[k-3] &&
			   ref> inputs[input_pos%INPUT_COUNT].magn[k-4] &&
			   ref> inputs[input_pos%INPUT_COUNT].magn[k+1] &&
			   ref> inputs[input_pos%INPUT_COUNT].magn[k+2] &&
			   ref> inputs[input_pos%INPUT_COUNT].magn[k+3] &&
			   ref> inputs[input_pos%INPUT_COUNT].magn[k+4]) {
			   if(peak_pos<MAX_PEAKS)
			   {
				   inputs[input_pos%INPUT_COUNT].peaks[peak_pos] = k;
			   }
			   peak_pos++;   
			}
		}
		inputs[input_pos%INPUT_COUNT].peaks_count = peak_pos;
		inputs[input_pos%INPUT_COUNT].valid = 1;
		fprintf(debugfile,"\n");
		fflush(debugfile);
		
		#ifdef SF_DEBUG
		sf_write_double (outfile, waveform, buffer_size*INPUT_COUNT);
		sf_close(outfile);
		#endif
		
		
	}
	  
}

