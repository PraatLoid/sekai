#ifndef SYNTH
#define SYNTH
#include <vector>
#include "sekai/VoiceSampler.h"
#include "sekai/VoiceDefESPEAK.h"

#define MAX_POLYPHONY 8

class SynthRT: public VoiceSampler
{ 
    public:
     SynthRT();
     VoiceDefESPEAK* voicedef;
     float period;
     bool enabled;
     float* impulse_response;
     int impulse_response_length;
     int samplerate;
    protected:
     bool addOnePulse();
};

class Synth
{
	public:
	Synth();
	~Synth();
	void init(int samplerate,int buffer_size);
	void noteOn(int notenum,int velocity);
	void noteOff(int notenum);
	void controllerEvent(int a1,int a2);
	void pitchBend(int a1,int a2);
	void fill(float* samples,int count);
	private:
	int samplerate=0;
    int buffer_size=0;
    
    char key[MAX_POLYPHONY];
    float freq[MAX_POLYPHONY];
    SynthRT* voice[MAX_POLYPHONY];
    
    int active_keys=0;
    
    
    //VoiceDefESPEAK* voicedef;
	
	
	float bend=0;
	float volume=1.0;
	int speed=64;
	

    
    
		
        
    
};
#endif
