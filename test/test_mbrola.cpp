#include <sekai/VoiceDefMBROLA.h>
#include <sekai/VoiceSampler.h>
#include <sndfile.h>

int g_samplerate = 0;

float interp_linear(float *x, float *y, int nx, float ref) {
  int i;
  for (i = 0; i < nx - 1; i++) {
    if (ref >= x[i] && ref <= x[i + 1]) {
      float x1 = x[i];
      float x2 = x[i + 1];
      float tmp = (ref - x1) / (x2 - x1);
      return y[i] * (1 - tmp) + y[i + 1] * tmp;
    }
  }
  fprintf(stderr, "INTERP_LINEAR: out of range\n");
  return NAN;
}

// new ;
class TestSynth : public VoiceSampler {
 public:
  TestSynth() : VoiceSampler(8192) {
    vox = new VoiceDefMBROLA(
        "/home/isengaara/Hacking/Audio/VoiceSynth/MBROLA/MBROLA-voices/tmp/de1/"
        "de1");
    g_samplerate = vox->getSamplerate();
    diph[0] = vox->getDiphone("d-o:");
    diph[1] = vox->getDiphone("R-e:");
    diph[2] = vox->getDiphone("m-I");
    printf("diph %p %p %p\n", diph[0], diph[1], diph[2]);
    _impulseResponse = new float[1024];
  }

 protected:
  virtual bool addOnePulse() {
    int samplerate = vox->getSamplerate();

    float currentTime = inputPositionSamples() / samplerate;
    int index = (int)currentTime;
    if (index == 3) return false;
    // get impulse response

    float otoTime = currentTime;
    while (otoTime > 1.0) otoTime -= 1.0;

    diphone *current_diph = diph[index];

    float diph_start = current_diph->begin * 1.0 / samplerate;
    float diph_middle = current_diph->middle * 1.0 / samplerate;
    float diph_end = current_diph->end * 1.0 / samplerate;

    printf("DIPH %f\n", diph_start - diph_middle);

    float x[3] = {0, diph_middle - diph_start, 1.0};
    float y[3] = {diph_start, diph_middle, diph_end};
    float index_time = interp_linear(x, y, 3, otoTime);

    int impulseResponseLength = 0;
    vox->getImpulseResponse(index_time, _impulseResponse,
                            &impulseResponseLength,0);

    float f0[3] = {261.63, 293.67, 329.63};
    float output_f0 = f0[index];
    float period = samplerate * 1.0f / output_f0;

    if (otoTime > 0.95) impulseResponseLength = 0;

    VoiceSampler::hanningWindow(_impulseResponse, impulseResponseLength);
    ola(_impulseResponse, impulseResponseLength, period);

    return true;
  }

 private:
  VoiceDefMBROLA *vox;
  diphone *diph[3];
  float *_impulseResponse;
};

int main() {
  TestSynth *synth = new TestSynth();

  SF_INFO info = {0};
  info.samplerate = g_samplerate;
  info.channels = 1;
  info.format = SF_FORMAT_WAV | SF_FORMAT_PCM_16;
  SNDFILE *sf = sf_open("/tmp/test_mbrola.wav", SFM_WRITE, &info);

  while (1) {
    const int size = 1024;
    int fill = size * 4;

    float buffer_out[size];
    if (synth->readData(buffer_out, size, fill) == false) break;
    sf_write_float(sf, buffer_out, size);
  }

  sf_close(sf);
}
