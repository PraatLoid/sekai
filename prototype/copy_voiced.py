# copy syntheis, ignore unvoiced part of the signal
import numpy as np
import soundfile as sf
import os
import synth_common

wav = "/home/isengaara/Hacking/Audio/QTAU/qtau/get_utau/SAKURA/sakura_01.wav"
					
h0,d0 = synth_common.readTrack(os.path.splitext(wav)[0]+".f0")  # pps voiced pitch
h1,d1 = synth_common.readTrack(os.path.splitext(wav)[0]+".pmk") # pos voiced 0

x, fs = sf.read(wav)

y = np.zeros(len(x))

for p in d1:
	f0 = synth_common.getPitch(d0,d1,p)
	if(f0>0):
		win,idx = synth_common.calculate_windowed_waveform(x,fs,f0,p[0])
		y[idx - 1] += win
		
z = x-y

sf.write('test.wav', y, fs)
sf.write('test2.wav', z, fs)

#for i in range(0,10):
#	print(getPitch([[0.0,1,100.0],[1.0,1,200]],None,[0.1*i,1,0]))


