#include "sekai/UnisynIndex.h"
#include "sekai/SynthInterface.h"

#include <fstream>
#include <iostream>
#include <sstream>

#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/lexical_cast.hpp>



bool UnisynIndex::readFromFile(std::string fileName) {
  //XXX: return false if file does not exist
  std::ifstream infile(fileName);

  bool header = true;
  std::string line;
  while (std::getline(infile, line)) {
    if (line == "EST_Header_End")
      header = false;
    else {
      std::vector<std::string> spl;
      boost::split(spl, line, boost::is_any_of("\t "),
                   boost::token_compress_on);
      if (header) {
        headerMap[spl[0]] = spl[1];
      } else {
        PhoInfo* p=new PhoInfo();
        p->basename = spl[1];
        
        for(int i=2;i<spl.size();i++)
        {
            p->alignment.push_back(boost::lexical_cast<float>(spl[i]));
        }
        
        if(preproc)
        {
            preproc->processPho(spl[0],p);
        }
        
        phoMap[spl[0]] = p;
        
      }
    }
  }

  return true;
}

 

//Pitch Track::pitchEmpty = {0};
