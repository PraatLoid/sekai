/*
  Copyright 2011 Tobias Platen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef JSONREADER_H
#define JSONREADER_H

#include <string>

class JSONReader {
 private:
  int dataLength;
  float *data;

  int pulseLocationsCount;
  int *pulseLocations;

  float f0;
  int samplerate;

 public:
  bool readFile(std::string);
  bool copyIn(float *buffera, float *bufferb, float *ratio, int buffer_length,
              float pos);
  int getSamplerate() { return samplerate; }
  float getPitch() { return f0; }
  float getLength() { return dataLength * 1.0 / samplerate; }
};

#endif
