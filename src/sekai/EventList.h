#ifndef SEKAI_EVENTLIST_H
#define SEKAI_EVENTLIST_H

#include <math.h>

class VoiceDef;

#define MAX_POINTS 64

struct PhoEvent {
  struct PhoEvent* next;
  VoiceDef* voice;
  int points;
  float x[MAX_POINTS];
  float y[MAX_POINTS];
  // needed for eventList
  inline float start() { return x[0]; }
  inline float end() { return x[points - 1]; }
};

template <typename T>
class EventList {
 public:
  void addEvent(T* n) {
    n->next = nullptr;

    if (_head == nullptr) {
      _head = n;
      _root = n;
    }
    if (_tail) {
      _tail->next = n;
    }
    _tail = n;
  }
  T* current() { return _head; }
  T* next() { return _head ? _head->next : nullptr; }
  void selectNext(float currentTime) {
    if (_head && currentTime >= _head->end()) _head = _head->next;
  }

 private:
  T* _head = nullptr;
  T* _root = nullptr;
  T* _tail = nullptr;
};

#endif
