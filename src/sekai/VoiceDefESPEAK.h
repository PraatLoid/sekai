/*
  Copyright 2011-2020 Tobias "Tomoko" Platen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VOICEDEF_ESPEAK_H
#define VOICEDEF_ESPEAK_H

#include <sekai/VoiceSampler.h>

#include <string>
#include <vector>

struct segment
{
    uint8_t type;
    uint32_t start;
    uint32_t length;
};

struct pho_event
{
    std::string code;
    uint32_t type;
    uint32_t start;
};

class VoiceDefESPEAK : public VoiceDef {
 public:
  VoiceDefESPEAK(std::string path);
  virtual ~VoiceDefESPEAK();
  void getImpulseResponse(float pos, float *impulseResponse,
                          int *impulseResponseLength,float morph);
  float getLength();
  int getSamplerate() { return _samplerate; }
  virtual std::string getPhoLine(int index);
  
 

 private:
  std::vector<short> _samples;
  std::vector<pho_event> _pho_events;
  std::vector<segment> _segments;
  
  uint32_t _samplerate = 0;
  uint32_t _period = 0;
};

// realtime_demo needs upgrade

#endif
