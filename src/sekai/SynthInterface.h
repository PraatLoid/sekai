#ifndef SEKAI_SYNTH_INTERFACE
#define SEKAI_SYNTH_INTERFACE

struct PhoInfo {
  std::string basename;
  std::vector<float> alignment;
  //add other optional stuff (mbrola only)
};

class IUnisyn 
{
  public:
        virtual void addUnit(const std::string& fileName,int count,float* a,float* b)=0;
        virtual float getLengthForUnit(const std::string& fileName)=0;
        virtual std::string getPhoLine(const std::string& fileName,int index)=0;
        virtual PhoInfo* lookupDiphone(const std::string& diph,float la,float lb)=0;
        virtual bool loadMBROLA(const std::string& path)=0;
};
#endif
