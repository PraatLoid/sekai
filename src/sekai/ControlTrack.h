#ifndef SINSY_CONTROL_TRACK_H_
#define SINSY_CONTROL_TRACK_H_

#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>
#include <vector>

struct GSLInterp
{
  float startPos;
  float endPos;
  gsl_interp_accel *acc;
  gsl_spline *cspline; 
};

struct DynSegment
{
    float start;
    float end;
    float dyn0;
    float dyn1;
};

class ControlTrack {
 public:
  //F0 control -- a spline is used here
  void addNote(float start, float length, float f0);
  void addPoint(float pos,float f0);
  void fix();
  float getF0atTime(float time);
  float getLength();
  void append(float delta);
  // dynamics -- linear interpolation is used here
  inline void addDynamicsSegment(const DynSegment& d) { dyn.push_back(d); }  
  float getDynamicsAtTime(float time);

 private:
  std::vector<double> time;
  std::vector<double> pitch;
  GSLInterp interp;
  float lastpos=0;
  std::vector<DynSegment> dyn;
};

#endif
