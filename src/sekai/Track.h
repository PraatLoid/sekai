#ifndef SEKAI_TRACK_H
#define SEKAI_TRACK_H

#include <sekai/common.h>
#include <map>
#include <string>
#include <vector>

struct Pitch {
  float pos;
  float voiced;
  float f0;
};

class Track {
 private:
  std::map<std::string, std::string> headerMap;
  std::vector<Pitch> pitchTrack;
  static Pitch pitchEmpty;

 public:
  bool readFromFile(std::string fileName);
  inline Pitch getPitch(unsigned int i) {
    if (unlikely(i > pitchTrack.size()))
      return pitchEmpty;
    else
      return pitchTrack[i];
  }
  inline int getPitchCount() { return pitchTrack.size(); }
  inline std::string getHeaderInfo(std::string field) { return headerMap[field]; }

  static void WriteF0(const char *filename, int f0_length, double frame_period,
  const double *temporal_positions, const double *f0, int text_flag);
};

#endif
