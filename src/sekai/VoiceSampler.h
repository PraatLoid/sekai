/*
  Copyright 2011 Tobias "Tomoko" Platen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VOICESAMPLER_H
#define VOICESAMPLER_H

#include <math.h>
#include <stdio.h>
#include <cstdlib>
#include <cstring>
#include "sekai/OLABuffer.h"
#include "sekai/Track.h"

class VoiceDef {
public:
  virtual float getLength() = 0;
  virtual int getSamplerate() = 0;
  virtual void getImpulseResponse(float pos, float *impulseResponse, int *impulseResponseLength,float morph) = 0;
  virtual std::string getPhoLine(int index=0) = 0;
};

class VoiceSampler : public OLABuffer {
 public:
  VoiceSampler(int bufferLen);
  virtual ~VoiceSampler();

  bool readData(float* data, int length, int fill);

 protected:
  virtual bool addOnePulse() = 0;
  virtual void postProcess(float* data, int size){}
  int outputPos=0;
  
  static void hanningWindow(float* signal, int size);
};

// realtime_demo needs upgrade

#endif
