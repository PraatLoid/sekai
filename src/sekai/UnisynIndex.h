#ifndef SEKAI_UNISYN_INDEX_H
#define SEKAI_TRACK_H

#include <sekai/common.h>
#include <map>
#include <string>
#include <vector>

class PhoInfo;
class VoiceDefMBROLA;

class UnisynPreproc {
    public:
      virtual bool processPho(const std::string& key,PhoInfo* pho)=0;
};

class UnisynIndex {
 private:
  std::map<std::string, std::string> headerMap;
  std::map<std::string, PhoInfo*> phoMap;
  UnisynPreproc* preproc=nullptr;

 public:
  UnisynIndex(UnisynPreproc* preproc=nullptr){this->preproc=preproc;}
  bool readFromFile(std::string fileName);
  inline PhoInfo* getPho(std::string pho) {
     return phoMap[pho];
  }
  inline void setPho(std::string pho,PhoInfo* p) {
     phoMap[pho] = p;
  }
  
};

#endif

