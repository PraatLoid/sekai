/*
  Copyright 2011 Tobias "Tomoko" Platen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VOICEDEF_MBROLA_H
#define VOICEDEF_MBROLA_H

#include <sekai/VoiceSampler.h>
#include <map>
#include <string>

class UnisynIndex;

struct diphone {
  int begin;
  int middle;
  int end;
};

struct diphone_mbr3
{
  std::string left;
  std::string right;
  uint32_t builder_pos;
  uint32_t builder_wpos;
  int16_t new_halfseg;
  uint8_t new_nb_frame;
  uint8_t nb_wframe;
  uint8_t* pmark;//count==new_nb_frame
  short* wav;//count==nb_wframe
};

struct diphone_alias {
  std::string left;
  std::string right;
  std::string left2;
  std::string right2; 
};

class VoiceDefMBROLA : public VoiceDef {
 public:
  VoiceDefMBROLA(std::string path);
  VoiceDefMBROLA(int fs,int mbrperiod);
  virtual ~VoiceDefMBROLA();
  void getImpulseResponse(float pos, float* impulseResponse,
                          int* impulseResponseLength,float morph);
  float getLength();

  diphone* getDiphone(std::string index);
  int getSamplerate() { return _samplerate; }
  virtual std::string getPhoLine(int index){return "";}
  void fillIndex(UnisynIndex* index);
  
  void writeToFile(std::string path);
  void distort();
  void addDiphone(diphone_mbr3* diph);

 private:
  short* _voice_data;
  uint32_t _voice_data_length;
  std::map<std::string, diphone*> _diphone_map;
  int _samplerate;
  int _mbrperiod;
  
  //header
  std::string _magic;
  std::string _version;
  int16_t _nb_diphone = 0;
  uint32_t _sizemark = 0;
  uint16_t _oldsizemark = 0;
  int32_t _sizeraw = 0;
  int16_t _samplerate16 = 0;
  uint8_t _mbrperiod8 = 0;
  uint8_t _coding = 0;
  
  std::vector<diphone_mbr3*> _diphone_new_vec;
  std::vector<diphone_alias*> _diphone_alias_vec;
  
  uint32_t _pmk_size=0;
  uint8_t* _pmk=nullptr;
  
  uint32_t _builder_pos=0;
  uint32_t _builder_wpos=0;
  
  //TODO: add tails
  
  
};

#endif
