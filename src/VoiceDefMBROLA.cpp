/*
  Copyright (c) 1995-2018 Faculte Polytechnique de Mons (TCTS lab)
  Copyright 2020 Tobias "Tomoko" Platen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "sekai/VoiceDefMBROLA.h"
#include "sekai/UnisynIndex.h"
#include "sekai/SynthInterface.h"
#include <stdio.h>

#include <sndfile.h>
#include <assert.h>

static std::string readstr(FILE* f, int length) {
  char str[length + 1];
  fread(str, 1, length, f);
  str[length] = 0;
  return str;
}

static void writestr(FILE* f,const std::string str)
{
    fwrite(str.c_str(), 1, str.size(), f);
}

static std::string readzstr(FILE* f) {
  std::string ret;
  while (1) {
    char c = fgetc(f);
    if (c == 0) break;
    ret += c;
  }
  return ret;
}

static void writezstr(FILE* f,const std::string str)
{
    fwrite(str.c_str(), 1, str.size()+1, f);
}

static void read_uint16(FILE* f, uint16_t* ret, int count) {
  fread(ret, count, sizeof(*ret), f);
}

static void write_uint16(FILE* f, uint16_t* ret, int count) {
  fwrite(ret, count, sizeof(*ret), f);
}

static void read_uint32(FILE* f, uint32_t* ret, int count) {
  fread(ret, count, sizeof(*ret), f);
}

static void write_uint32(FILE* f, uint32_t* ret, int count) {
  fwrite(ret, count, sizeof(*ret), f);
}

static void read_int16(FILE* f, int16_t* ret, int count) {
  fread(ret, count, sizeof(*ret), f);
}

static void write_int16(FILE* f, int16_t* ret, int count) {
  fwrite(ret, count, sizeof(*ret), f);
}


static void read_int32(FILE* f, int32_t* ret, int count) {
  fread(ret, count, sizeof(*ret), f);
}

static void write_int32(FILE* f, int32_t* ret, int count) {
  fwrite(ret, count, sizeof(*ret), f);
}

static void read_uint8(FILE* f, uint8_t* ret, int count) {
  fread(ret, count, sizeof(*ret), f);
}

static void write_uint8(FILE* f, uint8_t* ret, int count) {
  fwrite(ret, count, sizeof(*ret), f);
}

VoiceDefMBROLA::VoiceDefMBROLA(std::string path) {
  FILE* database = fopen(path.c_str(), "rb");

  _magic = readstr(database, 6);
  _version = readstr(database, 5);
#ifdef DUMP_DB
  printf("magic %s\n", _magic.c_str());
  printf("version %s\n", _version.c_str());
#endif

  _nb_diphone = 0;
  read_int16(database, &_nb_diphone, 1);
#ifdef DUMP_DB
  printf("nb_diphone %i\n", _nb_diphone);
#endif

  _sizemark = 0;
  _oldsizemark = 0;
  read_uint16(database, &_oldsizemark, 1);
  if (_oldsizemark == 0) {
    read_uint32(database, &_sizemark, 1);
  } else {
    _sizemark = _oldsizemark;
  }
#ifdef DUMP_DB
  printf("sizemark %i\n", _sizemark);
#endif

  //int32_t sizeraw = 0;
  //int16_t samplerate = 0;
  read_int32(database, &_sizeraw, 1);
  read_int16(database, &_samplerate16, 1);
#ifdef DUMP_DB
  printf("samplerate %i\n", _samplerate16);
#endif
  _samplerate = _samplerate16;

  read_uint8(database, &_mbrperiod8, 1);
  read_uint8(database, &_coding, 1);

  _mbrperiod = _mbrperiod8;

  int32_t indice_pm = 0;  /* cumulated pos in pitch mark vector */
  int32_t indice_wav = 0; /* cumulated pos in the waveform dba */
  uint8_t nb_wframe;      /* Physical number of frame */

  std::string new_left;
  std::string new_right;
  int16_t new_halfseg=0;
  uint8_t new_nb_frame=0;
  int32_t new_pos_pm=0;
  int32_t new_pos_wave=0;

  int i = 0;
  for (i = 0; ((int)indice_pm != (int)_sizemark) && (i < _nb_diphone); i++) {
    new_left = readzstr(database);
    new_right = readzstr(database);
    
    diphone* diph = new diphone;
    //diphone_new* dn = new diphone_new;

    read_int16(database, &new_halfseg, 1);
    read_uint8(database, &new_nb_frame, 1);
    read_uint8(database, &nb_wframe, 1);
    
    //dn->new_halfseg = new_halfseg;
    //dn->new_nb_frame = new_nb_frame;
    //dn->nb_wframe = nb_wframe;

    new_pos_wave = indice_wav;
    indice_wav += (long)nb_wframe * (long)_mbrperiod8;

#ifdef DUMP_DB
    printf("%i Diph [[%s-%s]] poswav=%li halfseg=%li pospm=%i nbframe=%i\n", i,
           new_left.c_str(), new_right.c_str(), new_pos_wave, new_halfseg,
           new_pos_pm, new_nb_frame);
#else
    (void)new_pos_pm;
#endif


    std::string diph_index = new_left + "-" + new_right;
    
    diph->begin = new_pos_wave;
    diph->middle = new_pos_wave + new_halfseg;
    diph->end = new_pos_wave + _mbrperiod8 * new_nb_frame;
    //dn->left = new_left;
    //dn->right = new_right;
    _diphone_map[diph_index] = diph;
    //_diphone_new_vec.push_back(dn);

    new_pos_pm = indice_pm;
    indice_pm += new_nb_frame;
  }

  for (; i < _nb_diphone; i++) {
    diphone_alias* diph = new diphone_alias;
    diph->left = readzstr(database);
    diph->right = readzstr(database);
    diph->left2 = readzstr(database);
    diph->right2 = readzstr(database);
    
    _diphone_alias_vec.push_back(diph);
    
    
    
    
#ifdef DUMP_DB
    printf("%i Diph [[%s-%s]] -> [[%s-%s]]\n", i, diph->left.c_str(), diph->right.c_str(),
           diph->left2.c_str(), diph->right2.c_str());
#endif
  }

  // read pitchmark
  _pmk_size = (_sizemark + 3) / 4; /* round to upper value */
  /* Compress 4 pitch marks in one byte */
  _pmk = new uint8_t[_pmk_size];
  fread(_pmk, sizeof(uint8_t), _pmk_size, database);

  _voice_data_length = _sizeraw / sizeof(short);
  _voice_data = new short[_voice_data_length];
  fread(_voice_data, sizeof(short), _voice_data_length, database);
#if 0
  SF_INFO info = {0};
  info.samplerate = samplerate;
  info.channels = 1;
  info.format = SF_FORMAT_WAV | SF_FORMAT_PCM_16;
  SNDFILE* sf = sf_open("/tmp/dump_mbrola.wav", SFM_WRITE, &info);
  sf_write_short(sf, _voice_data, _voice_data_length);
  sf_close(sf);
#endif

#if 0
  std::string end = readzstr(database);
//#ifdef DUMP_DB
  printf("end: %s", end.c_str());
//#endif
#endif

  fclose(database);
}

VoiceDefMBROLA::VoiceDefMBROLA(int fs,int mbrperiod)
{
    _samplerate = fs;
    _samplerate16 = fs;
    _mbrperiod8 = mbrperiod;
    _mbrperiod = mbrperiod;
    _magic = "MBROLA";
    assert(_magic.size()==6);
    _version = "3.000";
    _coding=1;
    assert(_version.size()==5);
}

//frame_lenght_samples = mbrperiod8 new_nb_frame;
//void VoiceDefMBROLA::addDiphone(std::string left,std::string right, int16_t new_halfseg, uint8_t new_nb_frame,uint8_t nb_wframe)
void VoiceDefMBROLA::addDiphone(diphone_mbr3* diph)
{
    assert(diph->nb_wframe*_mbrperiod<32767);
    
    diph->builder_pos =  _builder_pos;
    diph->builder_wpos = _builder_wpos;
    
    _builder_pos += diph->new_nb_frame;
    _builder_wpos += diph->nb_wframe;
    
    _diphone_new_vec.push_back(diph);
    _nb_diphone++;
    
    if(diph->left=="_" && diph->right=="_") {
        
        _sizemark = _builder_pos; //correct
        _sizeraw = sizeof(short)*_builder_wpos*_mbrperiod;
        printf("setting _sizemark %i sizeraw %i\n",_sizemark,_sizeraw);
        
        // read pitchmark
        _pmk_size = (_sizemark + 3) / 4; /* round to upper value */
        /* Compress 4 pitch marks in one byte */
        _pmk = new uint8_t[_pmk_size];
        memset(_pmk,0,_pmk_size);
        _voice_data_length = _sizeraw / sizeof(short);
        _voice_data = new short[_voice_data_length];
        
        //compress pitchmarks
        for(uint j=0;j<_diphone_new_vec.size();j++)
        {
            auto diph2 = _diphone_new_vec[j];
            for(uint i=0;i<diph2->new_nb_frame;i++)
            {
                int idx = diph2->builder_pos+i;
                int idx4 = idx/4;
                int mod4 = idx%4;
                uint8_t p = diph2->pmark[i];
                uint8_t p2 = p<<(2*mod4);
                //printf("idx4 %i %i %i %i %i %i\n",idx4,mod4,j,i,p,p2);
                _pmk[idx4] |= p2;
            }
            for(uint i=0;i<diph2->nb_wframe;i++)
            {
                int idx = diph2->builder_wpos+i;
                for(int k=0;k<_mbrperiod;k++)
                {
                    _voice_data[idx*_mbrperiod+k]=diph2->wav[i*_mbrperiod+k];
                }
            }
        }
        
    
       
        
    }
}

VoiceDefMBROLA::~VoiceDefMBROLA() {}

diphone* VoiceDefMBROLA::getDiphone(std::string index) {
  return _diphone_map[index];
}

void VoiceDefMBROLA::fillIndex(UnisynIndex* index)
{
	auto it = _diphone_map.begin();
    
    while (it != _diphone_map.end())
    {
		auto k = it->first;
		auto v = it->second;
		//printf("VoiceDefMBROLA::fillIndex %s\n",k.c_str());
		PhoInfo* p = new PhoInfo;
		float diph_start =  v->begin * 1.0 / _samplerate;
		float diph_middle = v->middle * 1.0 / _samplerate;
		float diph_end =    v->end * 1.0 / _samplerate;
		p->alignment.push_back(diph_start);
		p->alignment.push_back(diph_middle);
		p->alignment.push_back(diph_end);
		index->setPho(k, p);
		it++;
	}
}

void VoiceDefMBROLA::getImpulseResponse(float currentTime,
                                        float* impulseResponse,
                                        int* impulseResponseLength,float morph) {
  int samples = currentTime * _samplerate;
  *impulseResponseLength = 0;

  // reference
  int index = samples / _mbrperiod;
  int offset = index * _mbrperiod;

  uint len = 2 * _mbrperiod;

  for (uint i = 0; i < len; i++) {
    if (offset + i < _voice_data_length)
      impulseResponse[i] = _voice_data[offset + i] / 32768.0 * 0.9;
    else
      impulseResponse[i] = 0;
  }
  // TODO: add unvoiced part
  *impulseResponseLength = len;
}


float VoiceDefMBROLA::getLength()
{
    return 0;
}

void VoiceDefMBROLA::writeToFile(std::string path)
{
    printf("writing to file <%s>\n",path.c_str());
    FILE* database = fopen(path.c_str(), "wb");
    if(database) printf("have database\n");
    writestr(database,_magic);
    writestr(database,_version);
    write_int16(database, &_nb_diphone, 1);
    printf("sizemark %i %i\n", _sizemark,_oldsizemark);
    write_uint16(database, &_oldsizemark, 1);
    write_uint32(database, &_sizemark, 1);
    write_int32(database, &_sizeraw, 1);
    write_int16(database, &_samplerate16, 1);
    write_uint8(database, &_mbrperiod8, 1);
    write_uint8(database, &_coding, 1);
    // header done
    
    int i = 0;
    for (i = 0; i < (int)_diphone_new_vec.size(); i++) {
        auto diph = _diphone_new_vec[i];
        std::string new_left=diph->left;
        std::string new_right=diph->right;
        writezstr(database,new_left);
        writezstr(database,new_right);
        
        
        write_int16(database, &diph->new_halfseg, 1);
        write_uint8(database, &diph->new_nb_frame, 1);
        write_uint8(database, &diph->nb_wframe, 1);
    }
    
    for (i = 0; i < (int)_diphone_alias_vec.size(); i++) {
        auto diph = _diphone_alias_vec[i];
        writezstr(database,diph->left);
        writezstr(database,diph->right);
        writezstr(database,diph->left2);
        writezstr(database,diph->right2);
        }
        
    //index done
    fwrite(_pmk, sizeof(uint8_t), _pmk_size, database);
    fwrite(_voice_data, sizeof(short), _voice_data_length, database);
    
    fclose(database);
}
