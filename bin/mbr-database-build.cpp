#include <stdio.h>
#include <sekai/VoiceDefMBROLA.h>
#include <json/json.h>
#include <iostream>
#include <fstream>
#include <sekai/UnisynIndex.h>
#include <boost/filesystem.hpp>
#include <boost/range/iterator_range.hpp>
#include <iostream>
#include <sndfile.h>
#include <sekai/SynthInterface.h>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/lexical_cast.hpp>
#include <sekai/Track.h>

using namespace boost::filesystem;

#define KEY(s) static const std::string key_##s = #s;

KEY(fs)
KEY(mbrperiod)
KEY(index)

struct wav_data
{
    uint32_t count;
    short* data;
};

void loadWAV(const std::string& fn,int &fs,wav_data* wav)
{
    SF_INFO info;
    SNDFILE *sf = sf_open(fn.c_str(), SFM_READ, &info);
    if(sf==nullptr){
        printf("error: wav file <%s> does not exist\n",fn.c_str());
        exit(1);
    } 
    assert(info.channels==1);
    if(fs==0) fs = info.samplerate; else assert(info.samplerate);
    if(wav)
    {
        wav->count = info.frames;
        wav->data= new short[wav->count];
        sf_read_short(sf,wav->data,wav->count);
    }
    sf_close(sf);
}


class MBRPreproc: public UnisynPreproc
{
    int fs;
    int mbrperiod;
    int wav_fs=0;
    std::map<std::string,void*> phomap;
    std::map<std::string,Track*> trackmap;
    std::map<std::string,wav_data*> mbrmap;
    VoiceDefMBROLA* voice;
    public:
    MBRPreproc(int fs,int mbrperiod,VoiceDefMBROLA* voice){this->fs=fs; this->mbrperiod=mbrperiod;this->voice=voice;}
    bool processPho(const std::string& key,PhoInfo* pho)
    {
        if(pho->alignment.size()==3)
        {
            if(phomap[key]==nullptr)
            {
                std::string path = "wav/"+pho->basename+".wav";
                loadWAV(path,wav_fs,nullptr);
                
                if(trackmap[pho->basename]==nullptr)
                {
                    Track* track=new Track();
                    std::string f0_path = "f0/"+pho->basename+".f0";
                    track->readFromFile(f0_path);
                    trackmap[pho->basename]=track;
                }
                
                if(mbrmap[pho->basename]==nullptr)
                {
                    wav_data* mbr_data = new wav_data;
                    std::string mbr_path = "mbr/"+pho->basename+".wav";
                    loadWAV(mbr_path,fs,mbr_data);
                    mbrmap[pho->basename]=mbr_data;
                }
                
                
                
                //process alignment
                int start = pho->alignment[0]*fs/mbrperiod;
                int length = (pho->alignment[2]-pho->alignment[0])*fs/mbrperiod;
                int halfseg = (pho->alignment[1]-pho->alignment[0])*fs;
                //printf("mbrola format: start=%i length=%i halfset=%i wav_fs=%i\n",start,length,halfseg,wav_fs);
                assert(length<255); //mbrola uses 8bit data types here
                assert(halfseg<65535); //16 bit
                phomap[key]=(void*)0xFF;
                
                auto track = trackmap[pho->basename];
                auto mbr = mbrmap[pho->basename];
                double frame_shift = atof(track->getHeaderInfo("FrameShift").c_str());
                
                auto count = track->getPitchCount();
                
                std::vector<std::string> spl;
                boost::split(spl, key, boost::is_any_of("-"),boost::token_compress_off);
                assert(spl.size()==2);
                
                std::string left = spl[0];
                std::string right = spl[1];
                
                if(left=="#") left="_";
                if(right=="#") right="_";
                
                diphone_mbr3* diph=new diphone_mbr3;
                diph->left=left;
                diph->right=right;
                diph->new_halfseg = halfseg;
                diph->new_nb_frame = length;
                diph->nb_wframe = length+1;
                diph->pmark = new uint8_t[length];
                diph->wav = new short[diph->nb_wframe*mbrperiod]; 
                
                //copy pitch marks from track
                for(int i=0;i<length;i++)
                {
                    float pos = pho->alignment[0] + i*1.0*mbrperiod/fs;
                    float idx = pos/frame_shift;
                    int idx0=(int)(idx-0.5);
                    int idx1=(int)(idx+0.5);
                    assert(idx0<count);
                    assert(idx1<count);
                    //printf("idx[%i]: %f %f %i %i %i\n",i,pos,idx,idx0,idx1,count);
                    auto p0 = track->getPitch(idx0);
                    auto p1 = track->getPitch(idx1);
                    uint8_t flag=0;
                    if(p0.voiced || p1.voiced)  flag |= 1;
                    if(p0.voiced && !p1.voiced) flag |= 2;
                    if(!p0.voiced && p1.voiced) flag |= 2;
                    //printf("idx[%i]: flag=%i\n",i,flag);
                    diph->pmark[i]=flag;
                }
                
                //copy mbr_frames
                for(int i=0;i<length+1;i++)
                {
                    //printf("map_mbr: %i -> %i\n",start+i,i);
                    for(int j=0;j<mbrperiod;j++)
                    {
                        int idx_left = j+i*mbrperiod;
                        int idx_right = j+(start+i)*mbrperiod;
                        assert(idx_left<diph->nb_wframe*mbrperiod);
                        assert(idx_right<mbr->count);
                        diph->wav[idx_left] = mbr->data[idx_right];
                    }
                }
                
               
                
                if(left=="_" && right=="_") return true;
                
                voice->addDiphone(diph);
                
                
                
                //process pitchmarks  length
                //process frames      length+1
                //add to mbroladb
            }
            else
                printf("ignoring redunant %s\n",key.c_str());
        }
        return true;
    }
};

int main()
{
    int fs=16000;
    int mbrperiod=200;
    std::string index_file;
    
    
    //TOOD: json error handling
    Json::Value root;
    std::ifstream file("sekai.json");
    file >> root;
    
    if (root.isMember(key_fs) && root[key_fs].isInt())
    {
       fs = root[key_fs].asInt();
    }
    if (root.isMember(key_mbrperiod) && root[key_mbrperiod].isInt())
    {
        mbrperiod = root[key_mbrperiod].asInt();
    }
    if (root.isMember(key_index) && root[key_index].isString()) 
    {
        index_file = root[key_index].asString();
    }
    
    
    
    VoiceDefMBROLA* voice = new VoiceDefMBROLA(fs,mbrperiod);
    MBRPreproc* mbr=new MBRPreproc(fs,mbrperiod,voice);
    UnisynIndex* index= new UnisynIndex(mbr);
    
    printf("reading index file <%s>\n",index_file.c_str());
    if(index->readFromFile(index_file)==false)
    {
        printf("reading index file <%s> failed\n",index_file.c_str());
        return 1;
    }
    
    printf("adding silence at end\n");
    {
    diphone_mbr3* diph=new diphone_mbr3;
    diph->left="_";
    diph->right="_";
    diph->new_halfseg = mbrperiod*40;
    diph->new_nb_frame = 80;
    diph->nb_wframe = 81;
    diph->pmark = new uint8_t[80];
    diph->wav = new short[diph->nb_wframe*mbrperiod];
    memset(diph->pmark,0,80);
    memset(diph->wav,0,diph->nb_wframe*mbrperiod*sizeof(short));
    voice->addDiphone(diph);
    }
    
    printf("writing mbrola/voice");
    
    voice->writeToFile("mbrola/voice"); //TODO: path to output
    
}
